import UIKit



/*
    1. Написать простые классы с наследованием и без.
*/

class People {
    var name = "Name"
    var secondName = "Second name"
}

class Animals {
    var type: String
    var name: String

    init(type: String, nickname: String){
        self.type = type
        self.name = nickname
    }
}

class Man: People {

}

class Woman: People {

}

var animalsClass = Animals(type: "Dog", nickname: "Max")
animalsClass.type
animalsClass.name

var peopleClass = People()
peopleClass.name
peopleClass.secondName

var manClass = Man()
manClass.name = "Zinedine"
manClass.secondName = "Zidane"
var fullNameMan = manClass.name + " " + manClass.secondName

var womanClass = Woman()
womanClass.name = "Jessica"
womanClass.secondName = "Alba"
print("""
    The vomanClass variable contains an instance of the Voman class, which is a subclass of People and has two properties:
    1) Name: \(womanClass.name)
    2) Second name: \(womanClass.secondName)
    """)


/*
    2. Написать консольное приложение, в котором создать класс *House* и в нем несколько свойств - *width*, *height* и несколько методов - *build* (выводит на экран умножение ширины и высоты), *getWidth* и *getHeight* выводят на экран соответсвенно ширину и высоту.
*/


class House {
    var with: Double
    var height: Double

    init(with: Double, height: Double) {
        self.with = with
        self.height = height
    }
    func build() {
        print(self.with * self.height)
    }
    func getWith() {
        print(self.with)
    }
    func getHeight() {
        print(self.height)
    }

}

var houseClass = House(with: 25, height: 24.5)
houseClass.build()
houseClass.getWith()
houseClass.getHeight()



/*
    3. Написать класс , а в нем метод который будет принимать букву (одну, может быть и типа string, просто будете передавать ему одну букву) и возвращать все имена которые начинаются на эту букву.
    К примеру, А - Алексей, Александр, Артем, Антон и т. д. Внутри метода или полем класса (тут как удобно, не сильно критично) будет задаваться массив строк (string) в котором будут прописаны имена. Имена откуда-то подгружать не надо, их надо просто захардкодить. Метод должен возвращать отфильтрованный массив с именами.
    Так же написать метод, который будет принимать массив строк как аргумент и выводить их всех на консоль с новой строки каждое имя. Им распечатаете на консоль то что у вас получилось отфильтровать.
*/



class SearchName {
    var letter = ""
    var namesArray = ["Alex", "Remy", "Santos", "Ashley", "Nixon", "Vaughn", "Otto", "Audrey", "Anna", "Brianna", "Bella", "Ross", "Tia", "Howard", "Brooke", "Maxx", "Coen", "Bailey", "Caroline", "Clara", "Yuliana", "Willa", "Catherine", "Ann", "Yamileth", "Cora", "Danielle", "Jewel", "Diana", "Yahir", "Phillip", "Daniella", "Bree", "Evangeline", "Esther", "Faith", "Frances", "Giovanna", "Myra", "Martha", "Maryam", "Kaylyn", "Julianne", "Jenny", "Jovani", "Sonia", "Matilda", "Emmy", "Tegan", "Toby", "Hamza", "Ean", "Haylie", "Zachery", "Pearl", "Quintin", "Westin", "Seasmus", "Sergey", "Zuri", "Nahla", "Luz", "Nova"]

    func lookingForNames(letter: String, namesArray: [String]) -> [String] {        // A method that calculates names by the first letter of a name
        self.letter = letter
        self.namesArray = namesArray
        var endArray = [String]()

        guard letter.count == 1 else {
            return ["Only the first letter of the name must be entered"]
        }

        for name in namesArray {
            if letter.uppercased() == String(name[name.startIndex]) {
                endArray.append(name)
            }
        }
        return endArray
    }

    func nameFromNewLine(nameArray: [String]) {                                  // A method that prints each name from a new line
        for name in nameArray {
            print(name)
        }
    }
}

var search = SearchName()
search.letter = "y"

var filterNames = search.lookingForNames(letter: search.letter, namesArray: search.namesArray)
search.nameFromNewLine(nameArray: filterNames)





/*
    4) Написать класс, который формирует массив учеников, сортирует и считает количество этих учеников. Если учеников больше чем 30, выводится сообщение типа «в школе нет мест».
*/


class School {
    var learnersSchool = [String]()
    var count = 0

    func newStudentCame(name: String, surname: String) {        // A method that a creates new students and checks that their number does not exceed 30 students
        count += 1
        let fullName = name + " " + surname
        if count <= 30 {
            learnersSchool.append(fullName)
        } else {
            print("Sorry. There are no places at school")

        }

    }

    func filterLearner(arrayLearners: [String]) -> [String] {   // A method that sorts students
        return arrayLearners.sorted()
    }


}

var school = School()
school.count
school.newStudentCame(name: "Sergey", surname: "Marko")
school.newStudentCame(name: "Alex", surname: "Marko")
school.newStudentCame(name: "Caroline", surname: "Pit")
school.newStudentCame(name: "Sahsa", surname: "Faroul")

school.filterLearner(arrayLearners: school.learnersSchool)

    
    
/*
    5) Создать 5-10 своих структур.
*/


// created a structure to add two arguments of type String
struct Concatenation {
    var firstString: String
    var secondString: String

    func fullString() -> String {
        return self.firstString + " " + self.secondString

    }
}

var stroka = Concatenation(firstString: "Hello", secondString: "World")
stroka.fullString()

// created a structure that raises the number to a power
struct Exponentiation {
    var number: Int
    var exponent: Int

    func exp() -> Int {
        var temp = 1
        for _ in 1...self.exponent {
            temp *= self.number
        }
        return temp
    }

}

var number = Exponentiation(number: 35, exponent: 10)
number.exp()

/*
Created a structure that has 3 properties: size, color, material and displays a T-shirt with the specified properties in the console and displays the cost of the T-shirt depending on the size and material
*/
struct Tshirt {
    var size: String
    var color: String
    var material: String

    func cost() -> String {
        let sizeS = 5
        let sizeM = 7
        let sizeL = 10
        let materCotton = 5
        let materSynt = 3
        var answer = ""

        switch size.uppercased() {
        case "S":
            if material.lowercased() == "cotton" {
                answer = String(sizeS * materCotton)
            } else if material.lowercased() == "synthetics" {
                answer = String(sizeS * materSynt)
            }
        case "M":
            if material.lowercased() == "cotton" {
                answer = String(sizeM * materCotton)
            } else if material.lowercased() == "synthetics" {
                answer = String(sizeM * materSynt)
            }
        case "L":
            if material.lowercased() == "cotton" {
                answer = String(sizeL * materCotton)
            } else if material.lowercased() == "synthetics" {
                answer = String(sizeL * materSynt)
            }
        default:
            answer = "You entered the wrong size or material"
        }
        return answer
    }

    func buy() {
        if cost().count < 10 {
            print("You bought a T-shirt:\n\nThe size: \(size.uppercased())\nMaterial: \(material)\nColour: \(color)\nCost: \(cost()) \u{0024}")
        } else {
            print(cost())
        }

    }

}

var tShirt = Tshirt(size: "s", color: "Green", material: "Cotton")
tShirt.cost()
tShirt.buy()

// created a structure that displays the time of all the capitals of Europe
// To find out the time, you need to call the method and pass as the argument the name of the capital
// in which you know the time
struct TimeCapitalsEuro {
    let date = Date()
    let calendar = Calendar.current
    let utc0 = ["London", "Dublin", "Belfast", "Lisbon", "Reykjavik"]
    let utc1 = ["Oslo", "Berlin", "Paris", "Madrid", "Roma", "Vienna", "Amsterdam", "Brussels", "Warsaw", "Prague", "Vaduz", "Luxembourg", "Monaco", "Bern", "Budapest", "Bratislava", "Copenhagen", "Stockholm", "Tirana", "Andorra La Vella", "Sarajevo", "Valletta", "San-Marino", "Skopje", "Belgrade", "Ljubljana", "Zagreb", "Podgorica"]
    let utc2 = ["Kyiv", "Athens", "Vilnius", "Bucharest", "Riga", "Helsinki", "Sofia", "Kishinev", "Tallin"]
    let utc3 = ["Minsk", "Moscow", "Saint-Peterburg", "Istanbul", "Ankara"]

    func timeInCity(city: String) {

        if utc0.contains(city) {
            let hour = String(format: "%02d", calendar.component(.hour, from: date) - 3)
            let minute = String(format: "%02d", calendar.component(.minute, from: date))
            let second = String(format: "%02d", calendar.component(.second, from: date))
            print("Time in \(city): \(hour):\(minute):\(second)")
        } else if utc1.contains(city) {
            let hour = String(format: "%02d", calendar.component(.hour, from: date) - 2)
            let minute = String(format: "%02d", calendar.component(.minute, from: date))
            let second = String(format: "%02d", calendar.component(.second, from: date))
            print("Time in \(city): \(hour):\(minute):\(second)")
        } else if utc2.contains(city) {
            let hour = String(format: "%02d", calendar.component(.hour, from: date) - 1)
            let minute = String(format: "%02d", calendar.component(.minute, from: date))
            let second = String(format: "%02d", calendar.component(.second, from: date))
            print("Time in \(city): \(hour):\(minute):\(second)")
        } else if utc3.contains(city) {
            let hour = String(format: "%02d", calendar.component(.hour, from: date))
            let minute = String(format: "%02d", calendar.component(.minute, from: date))
            let second = String(format: "%02d", calendar.component(.second, from: date))
            print("Time in \(city): \(hour):\(minute):\(second)")
        } else {
            print("\(city) is not in Europe")
        }

    }


}

var time = TimeCapitalsEuro()
time.timeInCity(city: "Minsk")
time.timeInCity(city: "Athens")
time.timeInCity(city: "Paris")
time.timeInCity(city: "London")
time.timeInCity(city: "Tokio")



/*
    6) Сделайте список покупок! Программа записывает продукты в массив. Если вызываем определённый продукт — в консоли пишем к примеру «Мёд — куплено!».
*/

class BuyAtTheStore {
    var shoppingList = [String]()

    func buy(product: String) {                     // create a function which takes an argument of the type string("product") and if the array
        if !shoppingList.contains(product) {       // does not have this argument then it is added to the array, if there is then we output to the
            shoppingList.append(product)           // console the "prooduct - bought"
        } else {
            print("\(product) - bought")
        }
    }
}

var listProd = BuyAtTheStore()
listProd.buy(product: "Honey")
listProd.buy(product: "Milk")
listProd.buy(product: "Apples")
listProd.buy(product: "Bread")
listProd.buy(product: "Eggs")
listProd.shoppingList

listProd.buy(product: "Bread")


