/*
 Task 1
 
 Create a variable named counter and set it equal to 0.
 Create a while loop with the condition counter < 10 which prints out counter is X (where X is replaced with counter value) and then increments counter by 1.

 */


// create a variable of type: Int
var counter = 0
// create a while loop and in the body of the loop I print the a variable counter, then increase the value of the variable by 1
while counter < 10 {
    print(counter, terminator:" ")
    counter += 1
}


/*
 Task 2
 
 Create a variable named counter and set it equal to 0. Create another variable named roll and set it equal to 0.
Create a repeat-while loop. Inside the loop, set roll equal to Int.random(in: 0...5) which means to pick a random number between 0 and 5.
Then increment counter by 1. Finally, print After X rolls, roll is Y where X is the value of counter and Y is the value of roll.
Set the loop condition such that the loop finishes when the first 0 is rolled.

 */

// move the carriage position to the next line(for readability of the output to the console)
print("\n")

// create two variables of type: Int
var counter1 = 0
var roll = 0

// create a repeat - while loop
repeat {
    roll = Int.random(in: 0...5)
    counter1 += 1
    print("counter: \(counter1), roll: \(roll)")
} while roll != 0



/*
Task 3

Create a constant named range, and set it equal to a range starting at 1 and ending with 10 inclusive.
Write a for loop that iterates over this range and prints the square of each number.

 */

// move the carriage position to the next line(for readability of the output to the console)
print()

// create a constant of type: ClosedRange and create a for loop where prints the square of each number
let range: ClosedRange = 1...10
for number in range {
    print(number * number, terminator:" ")
}



/*
 Task 4
 
 Below you can see an example of for loop that iterates over only the even rows like so:
 */

var sum = 0
for row in 0..<8 where row % 2 != 0 {
    for column in 0..<8 {
        sum += row * column
    }
}
/*
 Change this to use a where clause on the first for loop to skip even rows instead of using continue. Check that the sum is 448 as in the initial example.
 */



/*
Task 5

Print a table of the first 10 powers of 2
*/

// create two constans of type Int and create a variable of type Int
let base = 2
let power = 10
var answer = 1

//print a value of 2 to the power of 0
print("\n\n\(answer)", terminator:" ")

// create a for loop which prints a value of 2 to the power of 1 to 10
for _ in 1...power {
    answer *= 2
    print(answer, terminator:" ")
}


/*
Task 6

Given a number n, calculate the factorial of n.
Example: 4 factorial is equal to 1 * 2 * 3 * 4
*/

//create a constant for number factorial of type Int, then create a variable to calculate the factorial value in a for loop
let factorialNumber = 5
var valueFactorial = 1

for n in 1...factorialNumber {
    valueFactorial *= n
}


/*
Task 7

Given a number n, calculate the n-th Fibonacci number. (Recall Fibonacci is 1, 1, 2, 3, 5, 8, 13, ... Start with 1 and 1 and add these values together to get the next value. The next value is the sum of the previous two. So the next value in this case is 8+13 = 21.)
*/

// create a constant of type Int and assign it a Fibonacci number
/*
 create three variables of type Int:
 valueNumberFibonacci - write the value here n-th Fibonacci number
 F[n] = F[n-1] + F[n-2], where
 previousFibonacciNimber_1 = F[n-1]
 previousFibonacciNimber_2 = F[n-2]
*/
let numberFiboncci = 6
var valueNumberFibonaci = 0
var previousFiboncciNumber_2 = 0
var previousFibonacciNumber_1 = 1

switch numberFiboncci {
case 0:
    // F[0] = 0
    valueNumberFibonaci = 0
case 1:
    // F[1] = 1
    valueNumberFibonaci = 1
default:
    var counter2 = 2
    while counter2 <= numberFiboncci {
        valueNumberFibonaci = previousFibonacciNumber_1 + previousFiboncciNumber_2
        previousFiboncciNumber_2 = previousFibonacciNumber_1
        previousFibonacciNumber_1 = valueNumberFibonaci
        counter2 += 1
    }
}
print("\n\n\(valueNumberFibonaci)")



/*
Task 8

Write a switch statement that takes an age as an integer and prints out the life stage related to that age. You can make up the life stages, or use my categorization as follows: 0-2 years, Infant; 3-12 years, Child; 13-19 years, Teenager; 20-39, Adult; 40-60, Middle aged; 61+, Elderly.
*/

var age = 35
var lifeStage = ""
switch age {
case 0...2:
    lifeStage = "Infant"
case 3...12:
    lifeStage = "Child"
case 13...19:
    lifeStage = "Teenager"
case 20...39:
    lifeStage = "Adult"
case 40...60:
    lifeStage = "Middle aget"
default:
    lifeStage = "Elderly"
}
print("\n\(lifeStage)")



/*
Task 9

Write a switch statement that takes a tuple containing a string and an integer. The string is a name, and the integer is an age. Use the same cases that you used in the previous exercise and let syntax to print out the name followed by the life stage. For example, for myself it would print out "Slava is an adult."
*/

let tupleLifeStage = ("Sergey", 30)
switch tupleLifeStage {
case (let name, 0...2):
    print("\n\(name) is an infant")
case (let name, 3...12):
    print("\n\(name) is a child")
case (let name, 13...19):
    print("\n\(name) is a teenager")
case (let name, 20...39):
    print("\n\(name) is an adult")
case (let name, 40...60):
    print("\n\(name) is a middle aget")
case (let name, _):
    print("\n\(name) is an elderly")
}



/*
 Task 10

Create a constant called myAge and set it to your age. Then, create a constant named isTeenager that uses Boolean logic to determine if the age denotes someone in the age range of 13 to 19.
*/

let myAge = 30
let isTeenager = myAge >= 13 && myAge <= 17



/*
  Task 11
 Create another constant named theirAge and set it to my age, which is 30. Then, create a constant named bothTeenagers that uses Boolean logic to determine if both you and I are teenagers.
 */

let theirAge = 30
let bothTeenagers = (myAge >= 13 && myAge <= 17) && (theirAge >= 13 && theirAge <= 17)



/*
 Task 12
Create a constant named reader and set it to your name as a string. Create a constant named author and set it to my name, Matt Galloway. Create a constant named authorIsReader that uses string equality to determine if reader and author are equal.
*/

let reader = "Sergey Marko"
let author = "Matt Galloway"
let authorIsReader = reader == author



/*
 Task 13

Создайте массив "дни в месяцах":
Распечатайте элементы, содержащие количество дней в соответствующем месяце, используя цикл for и этот массив.
*/

print()
var daysInMonths = Array(1...31)
for day in daysInMonths {
    print(day, terminator:" ")
}



/*
 Task 14
Создать в if и отдельно в switch программу которая будет смотреть на возраст человека и говорить куда ему идти в школу, в садик, в универ, на работу или на пенсию и тд.
*/

// create a constant of type Int and assign the age of a person
/* I also create a variable of type Bool to check whether a person studied well at school(this will help determine where he will go after school, to university or he will be drafted into the army)
 */
let agePerson = 18
var youStudiedWell = false

if agePerson >= 3 && agePerson < 6 {
    print("\n\nYou need to go kindergarten.")
} else if agePerson >= 6 && agePerson < 17 {
    print("\n\nYou need to go school.")
} else if agePerson >= 17 && agePerson < 22 {
    if youStudiedWell{
        print("\n\nYou need to go to university...=)")
    } else {
        print("\n\nYou were drafted into the army...=(")
    }
} else if agePerson >= 22 && agePerson < 63 {
    print("\n\nYou need to go to work.")
} else {
    print("\n\nNow you can relax, you are a senior citizen!!!")
}

// implement the solution through switch
let agePerson1 = 18
var youStudiedWell_1 = true

switch agePerson1 {
case 3..<6:
    print("\nYou need to go kindergarten.")
case 6..<17:
    print("\nYou need to go school.")
case 17..<22:
    if youStudiedWell_1{
        print("\n\nYou need to go to university...=)")
    } else {
        print("\n\nYou were drafted into the army...=(")
    }
case 22..<63:
    print("\n\nYou need to go to work.")
default:
    print("\n\nNow you can relax, you are a senior citizen!!!")
}


/*
 Task 15
В switch и отдельно в if создать систему оценивания школьников по 12 бальной системе и высказывать через print мнение.
*/


// create a constant of type Int for further knowledge assessment
let points = 10
switch points {
case 0:
    print("\nVery bad")
case 1...3:
    print("\nBad")
case 4...6:
    print("\nAverage")
case 7...9:
    print("\nWell")
case 10, 11:
    print("\nFine")
case 12:
    print("\nExcellently")
default:
    print("\nYou points is not correct")
}

// implement the solution through if

let points1 = 3

if points1 == 0 {
    print("\nVery bad")
} else if points1 >= 1 && points1 <= 3 {
    print("\nBad")
} else if points1 >= 4 && points1 <= 6 {
    print("\nAverage")
} else if points1 >= 7 && points <= 9 {
    print("\nWell")
} else if points1 == 10 || points1 == 11 {
    print("\nFine")
} else if points1 == 12 {
    print("\nExcellently")
} else {
    print("\nYou points is not correct")
}
