import UIKit
import Foundation
/*
    1. Write a function named printFullName that takes two strings called firstName and lastName.
 The function should print out the full name defined as firstName + " " + lastName.
 Use it to print out your own full name.
*/


func printFullName(firstName: String, lastName: String) {
    print(firstName + " " + lastName)
}

printFullName(firstName: "Sergey", lastName: "Marko")



/*
    2. Создайте функцию, которая принимает параметры и вычисляет площадь круга.
*/


// create a function of type Double -> Double which I pass as a parameter to the radius of the circle
// and return the value of the area of the circle

func areaCircle(radius: Double) -> Double {
    return Double.pi * pow(radius, 2.0)
}
print("")
print(areaCircle(radius: 2))



/*
    3. Создайте функцию, которая принимает параметры и вычисляет расстояние между двумя точками.
*/


typealias Point = (x: Double, y: Double)

func distBetween(pointOne a: Point, andPointTwo b: Point) -> Double {
    return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2))
}
print("")
print(distBetween(pointOne: (1, 0), andPointTwo: (-2, 0)))



/*
    4. Напишите функцию, котороя считает факториал числа.
*/

func factorialNumber(number num: Int) -> Int {
    guard num > 0 else { return num + 1 }
    return num * factorialNumber(number: num - 1)
}
print("")
print(factorialNumber(number: 5))



/*
    5. Напишите функцию, которая вычисляет N-ое число Фибоначчи
*/



func numberFibonacci(N number: Int) -> Int {
    guard number > 1 else { return number }
    return numberFibonacci(N: number - 1) + numberFibonacci(N: number - 2)
}
print("")
print(numberFibonacci(N: 4))



/*
    6. First, write the following function:

 func isNumberDivisible(_ number: Int, by divisor: Int) -> Bool

 You’ll use this to determine if one number is divisible by another. It should return true when number is divisible by divisor.

 Hint: You can use the modulo (%) operator to help you out here.

 Next, write the main function:

 func isPrime(_ number: Int) -> Bool

 This should return true if number is prime, and false otherwise. A number is prime if it’s only divisible by 1 and itself.
 You should loop through the numbers from 1 to the number and find the number’s divisors.
 If it has any divisors other than 1 and itself, then the number isn’t prime. You’ll need to use the isNumberDivisible(_:by:) function you wrote earlier.
 Use this function to check the following cases:

    isPrime(6) // false
    isPrime(13) // true
    isPrime(8893) // true

 Hint 1: Numbers less than 0 should not be considered prime. Check for this case at the start of the function and return early if the number is less than 0.
 Hint 2: Use a for loop to find divisors. If you start at 2 and end before the number itself, then as soon as you find a divisor, you can return false.
*/


func isNumberDivisible(_ number: Int, by divisior: Int) -> Bool {
    return number % divisior == 0
    
}

func isPrime(_ number: Int) -> Bool {
    guard number > 0 else { return false }
    
    for num in 2..<number {
        if isNumberDivisible(number, by: num) {
            return false
        }
    }
    return true
}
print("")
print(isPrime(9))
