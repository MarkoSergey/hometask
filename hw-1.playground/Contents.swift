import Foundation

                       /* 1. Константы, переменные и типы данных */

/* 1) Напишите переменные и константы всех базовых типов данных: int, UInt, float, double, string.
      У чисел вывести их минимальные и максимальные значения. */


// система имеет 64-битную разрядность

// создаю констануту и переменную типа: Int
let numberIntLet = -60
var numberIntVar = 0

// создаю константу и переменную типа: UInt
let numberUIntLet = 0
var numberUIntVar = 99

// создаю константу и переменную типа: Double
let numberDoubleLet : Double = 13.34565767585867
var numberDoubleVar : Double = -23.2453546455643

// создаю константу и переменную типа: Float
let numberFloatLet : Float = 4.56788
var numberFloatLVar : Float = -5.12567 - 5.65780

// создаю константу и переменную типа: String
let stringLet = "Hello World"
var stringVar = "Abra" + "Cadabra"

// создаю константу и переменную типа: Bool
let boolTrue = true
var boolFalse = false

// вывожу минимальное и максимальное значение у типа: Int
var numberIntMin = Int.min
var numberIntMax = Int.max

// вывожу минимальное и максимальное значение у типа: UInt
var numberUIntMin = UInt.min
var numberUIntMax = UInt.max

/* вывожу минимальное и максимальное значение между объявленными значениями
   переменной и константы типа: Double */
var numberDoubleMin = Double.minimum(numberDoubleVar, numberDoubleLet)
var numberDoubleMax = Double.maximum(numberDoubleVar, numberDoubleLet)

/* вывожу минимальное и максимальное значение между объявленными значениями
переменной и константы типа: Float */
var numberFloatMin = Float.minimum(numberFloatLVar, numberFloatLet)
var numberFloatMax = Float.maximum(numberFloatLVar, numberFloatLet)



/* 2) Создайте список товаров с различными характеристиками (количество, название).
      Используйте typealias. */



// создаю псевдоним типа:
typealias Quantity = UInt8

// создаю характеристики продукта
let theProductName  = "Milk"
var amountOfProduct = (Quantity.max)
let prodictionDate  = "25/06/2019"
let shelfLife       = "Best before: 24/07/2019"

// упаковываю характеристики в кортеж для удобочитаемости
let productSupply = (theProductName, "= \(amountOfProduct)", prodictionDate, shelfLife)

// вывод
print("\(productSupply)\n")



/* 3) Напишите различные выражения с приведением типа. */



// сложение чисел разных типовпри объявлении переменной и передаче ей значения
var summaDoubleNumbers = 3 + 3.345345 + 55

// обьявляю переменные разных типов
var intNumber = 34
var floatNumber : Float = 5.55664

// приведение из типа "Float" в "Int"
var sumIntNumPlusFloatNum = intNumber + Int(floatNumber)
// приведение из типа "Int" в "Float"
var sumFloatNumMinusIntNum = Float(intNumber) - floatNumber

// объявляю пременные разных типов
var numberInt8   : Int8   = 15
var numberUInt16 : UInt16 = 100

// приведение из типа "UInt16" в "Int8"
var numInt8MinusNumUInt16 = numberInt8 - Int8(numberUInt16)
// приведение из типа "Int8" в "UInt16"
var numUInt16PlusNumInt8 = numberUInt16 + UInt16(numberInt8)



/* 4) Вычисления с операторами (умножение, деление, сложение, вычитание)
      Результат вычислений должен выводиться в консоль в таком виде: «3 + 2 = 5». */



// объявляю переменные
var three = 3
var four  = 4

// операция сложения двух чисел
var additionOfTwoNumbers = "\(three) + \(four) = \(three + four)"
// операция вычитания двух чисел
var subtractionOfTwoNumbers = "\(three) - \(four) = \(three - four)"
// операция умножеиня двух чисел
var multiplicationOfTwoNumbers = "\(three) * \(four) = \(three * four)"
// операция деления двух чисел
var divisionOfTwoNumbers = "\(three) / \(four) = \(Double(three) / Double(four))"

// вывод значений в консоль
print(additionOfTwoNumbers)
print(subtractionOfTwoNumbers)
print(multiplicationOfTwoNumbers)
print(divisionOfTwoNumbers)



/* 5) Declare two constants a and b of type Double and assign both a value. Calculate the average   of a and b and store the result in a constant named average. */



// declare constants "a" and "b" of type "Double"
let a = 9.998877
let b = 4.332299

// calculate the average value of "a" and "b"
let average = (a + b) / 2



/* 6) A temperature expressed in °C can be converted to °F by multiplying by 1.8 then incrementing by 32. In this challenge, do the reverse: convert a temperature   from °F to °C. Declare a constant named fahrenheit of type Double and assign it a value. Calculate the corresponding temperature in °C and store the result    in a constant named celcius. */



// declare constant of type "Double"
let fahrenheit = 89.60

//temperature conversion from °F to °C
let celcius = (fahrenheit - 32) / 1.8



/* 7) A circle is made up of 2𝜋 radians, corresponding with 360 degrees. Declare a constant degrees of type Double and assign it an initial value. Calculate the corresponding angle in radians and store the result in a constant named radians. */



// declare constant of type "Double"
let degrees = 122.0

// value conversion from degrees to radians
let radians = degrees * (Double.pi / 180)



/* 8) Declare four constants named x1, y1, x2 and y2 of type Double. These constants represent the 2-dimensional coordinates of two points. Calculate the distance   between these two points and store the result in a constant named distance. */



// declare constans x1, y1, x2, y2
let x1 = 2.0, y1 = 4.0
let x2 = -1.0, y2 = -2.0

// find the distance between the points
let distance = sqrt(pow((x1 + abs(x2)), 2) + pow((y1 + abs(y2)), 2))


                      
                           /* 2. Строки */



/* 1) Напишите с помощью строк своё резюме: имя, фамилия, возраст, где живете, хобби и т.п. */



// объявляю константы и передаю им значение

var myName     = "Sergey"
var secondName = "Marko"
var age        = "30"
var address    = "Republic of Belarus, Minsk, st. Kahovskaya, 66, apt. 80"
var hobby      = "Sports and Rap"



/* 2) Соберите из этих строк 1 большую (вспоминаем интерполяцию) и выведите в консоль. */



print("\nHi!\nMy name is \(secondName) \(myName).\nI am \(age) ears old.\nI live by address \(address).\nI'm interested in \(hobby).\n\nGoodbye!")



/* 3) Напишите 10 строк, соберите их с помощью интерполяции и поставьте в нужных местах переносы на новую строку и пробелы (см. \n и \t). */


// объявляю 10 констант и передаю им значения типа: String
let firstString   = "Three tomatoes are walkin' down the street."
let secondString  = "Papa Tomato"
let thirdString   = "Mama Tomato"
let fourthString  = "Baby Tomato"
let fifthString   = "Baby Tomato starts lagging behind"
let sixthString   = "and Papa Tomato gets really angry."
let seventhString = "Goes back"
let eighthString  = "and"
let ninthString   = "squishes him and says:"
let tenthString   = "'Catch up'"

print("\n\n\(firstString)\t\(secondString), \(thirdString) and \(fourthString).\n\(fifthString), \(sixthString) \(seventhString)\t\(eighthString)\t\(ninthString) \(tenthString).")



/* 4) Разбейте собственное имя на символы, перенося каждую букву на новую строку. */


print("\nS\ne\nr\ng\ne\ny")


/* 5) Создайте переменную типа Int и переменную типа String.
      Тип Int преобразуйте в String и с помощью бинарного оператора сложите 2 переменные в одну строку. */

// создаю переменные
var date   = 1
var mounth = " January"

//преобразовываю переменную типа "Int" в тип "String" и складываю эти переменные
var newYear = String(date) + mounth



/* 6) Create a string constant called firstName and initialize it to your first name. Also create a string constant called lastName and initialize it to your last   name. */


// create constans of type "String" and pass them my name and surname
let firstName = "Sergey"
let lastname  = "Marko"



/* 7) Create a string constant called fullName by adding the firstName and lastName constants together, separated by a space. */

// create constant
let fullName = firstName + "\t" + lastname


/* 8) Using interpolation, create a string constant called myDetails that uses the fullName constant to create a string introducing yourself. For example, my string would read: "Hello, my name is Matt Galloway." */


// create constant of type "String"
let myDetails = "Hi! My name is \(fullName)"



                        
                       /* 3. Tuples */



/* 1) Создать кортеж с 3-5 значениями. Вывести их в консоль 3 способами. */


// объявляю переменную и передаю ей кортеж
var tupl = ("Sergey", 12, 45.1234, true)

// вывод в консоль: 1-й способ
print("\n\(tupl.0)")
print(tupl.1)
print(tupl.2)
print(tupl.3)

// передаю имена элементам кортежа
var tupl1 = (name: "Sergey", temperature: 12, doubleNumber: 45.1234, value: true)

// вывод в консоль: 2-й способ
print("\n\(tupl1.name)")
print(tupl1.temperature)
print(tupl1.doubleNumber)
print(tupl1.value)

//раскладываю содержимое кортежа на отдельные константы
let (name, temperature, doubleNumber, value) = tupl

// вывод в консоль: 3-й способ
print("\n\(name)")
print(temperature)
print(doubleNumber)
print(value)



/* 2) Давайте представим, что мы сотрудник ГАИ и у нас есть какое-то количество нарушителей. Задача. Создать кортеж с тремя параметрами:
      - первый - превышение скорости: количество пойманных;
      - второй - вождение нетрезвым: количество пойманных;
      - третий - бесправники: количество пойманных.
      Распечатайте наших бедокуров в консоль через print(). */



// объявляю переменную и передаю ей кортеж
var firstTrafficPoliceOfficer = (overSpeed: 7, drunkDriving: 3, withoutRights: 4)
// вывод в консоль
print("\n\t\tFixing violation traffic police officer\n\nOver speed:\t\t\(firstTrafficPoliceOfficer.overSpeed)\nDrunk driving:  \(firstTrafficPoliceOfficer.drunkDriving)\nWithoutvrights: \(firstTrafficPoliceOfficer.withoutRights)")



/* 3) Выведите каждый параметр в консоль. Тремя способами. */



// вывод в консоль: 1-й способ
print("\n\(firstTrafficPoliceOfficer.overSpeed)")
print(firstTrafficPoliceOfficer.drunkDriving)
print(firstTrafficPoliceOfficer.withoutRights)

// вывод в консоль: 2-й способ
print("\n\(firstTrafficPoliceOfficer.0)")
print(firstTrafficPoliceOfficer.1)
print(firstTrafficPoliceOfficer.2)

// раскладываю содержимое кортежа на отдельные переменные
var (overSpeed, drunkDriving, withoutRights) = firstTrafficPoliceOfficer

// вывод в консоль: 3-й способ
print("\n\(overSpeed)")
print(drunkDriving)
print(withoutRights)



/* 4) Создайте второй кортеж — нашего напарника. Значения задайте другие. */


// объявляю переменную и передаю ей кортеж
var secondTrafficPoliceOfficer = (overSpeed: 5, drunkDriving: 6, withoutRights: 1)



/* 5) Пусть напарники соревнуются: создайте третий кортеж, который содержит в себе разницу между первым и вторым. */


// нахожу разницу между полицейскими
var ovSpeed     = firstTrafficPoliceOfficer.overSpeed - secondTrafficPoliceOfficer.overSpeed
var drunDriving = abs(firstTrafficPoliceOfficer.drunkDriving - secondTrafficPoliceOfficer.drunkDriving)
var withRights  = firstTrafficPoliceOfficer.withoutRights - secondTrafficPoliceOfficer.withoutRights

// объявляю переменную и передаю ей кортеж
var differenceBetweenTheCops = (ovSpeed, drunDriving, withRights)


/* 6) Declare a constant tuple that contains three Int values followed by a Double. Use this to represent a date (month, day, year) followed by an average temperature for that date. */


// create a constant and pass a tuple
let dateOf = (11, 18, 2019, 8.5)



/* 7) Change the tuple to name the constituent components. Give them names related to the data that they contain: month, day, year and averageTemperature. */


// change the tuple and give names to the components
let dateName = (mounth:11, day:18, year:2019,dayTemp:7, averageTemp:8.5)



/* 8) In one line, read the day and average temperature values into two constants. You’ll need to employ the underscore to ignore the month and year. */


// read the daily and average temperature
let (_, _, _, dayTemperature, averageTemperature) = dateName



/* 9) Up until now, you’ve only seen constant tuples. But you can create variable tuples, too. Change the tuple you created in the exercises above to a variable by using var instead of let. Now change the average temperature to a new value. */


// create a variable and pass a tuple
var dateVar = (mounth:11, day:18, year:2019,dayTemp:7, averageTemp:8.5)

// change the average temperature
dateVar.averageTemp = 7.7
