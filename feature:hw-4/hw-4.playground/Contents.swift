import UIKit

/* create two structures named "Car"(in it we willstore the properties and methods regarding the car) and
 "CarOwner"(in it we willstore the properties and methods regarding the owner of the car)
*/
struct Car {
    var brand: String
    var yearOfIssue: Int
    var color: String
    var typeOfBody: String
    var typeOfFuel: String
    
    func description() {                            // create a method that displays a description of the car in the console
        print("""
            Before you car:\n
            Brand: \(brand)
            Year of manufacture: \(yearOfIssue)
            Color: \(color)
            Type of body: \(typeOfBody)
            Type of fuel: \(typeOfFuel)
            """)
    }
}

struct CarOwner {
    var fullName: String
    var ageOfPerson: Int
    var country: String
    var car: Car
    
    func describeCar() {                          // create a method that displays a description of the owner car and a description of the car in the console
        print("""
            Car owner: \(fullName)
            Age: \(ageOfPerson)
            Country of Residence: \(country)
            
            \(fullName) sells a car brand: \(car.brand) with the following description:
            Year of manufacture: \(car.yearOfIssue)
            Color: \(car.color)
            Type of body: \(car.typeOfBody)
            Type of fuel: \(car.typeOfFuel)
            """)
    }
    
}
// create an instance Car
var car = Car(brand: "Mitsubishi Lancer Evolution X",
              yearOfIssue: 2008,
              color: "Yellow",
              typeOfBody: "Sedan",
              typeOfFuel: "Petrol")

// create an instance CarOwner
var owner = CarOwner(fullName: "Sammi Hachatori", ageOfPerson: 28, country: "USA", car: car)

car.description()
print("")
owner.describeCar()
